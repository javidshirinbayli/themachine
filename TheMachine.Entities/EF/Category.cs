﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMachine.Entities.EF
{
    public class Category:Base
    {
        public Category()
        {
            Products = new List<Product>();
        }
        [Required]
        public string CategoryName { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}
