﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMachine.Entities.EF
{
    public class Product :Base
    {
        [Required, StringLength(30)]
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        [Required]
        public int UnitInStock { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }

        public int Category_ID { get; set; }

        [ForeignKey("Category_ID")]
        public virtual Category Category { get; set; }
    }
}
