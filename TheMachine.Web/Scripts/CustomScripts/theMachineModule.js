﻿
var theMachineModule = angular.module("theMachineModule", ['ngMaterial']);
theMachineModule.directive('isNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope) {
            scope.$watch('wks.number', function (newValue, oldValue) {
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.')) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue)) {
                    scope.wks.number = oldValue;
                }
            });
        }
    };
});

var theMachineController = theMachineModule.controller("theMachineController", function ($scope, $http) {


    $scope.Result = false;
    $scope.Buy = function (_productId, money) {
        $http.get("http://localhost:2342/api/Product/ProductById/" + _productId)
         .success(function (data) {
             var newData = data;
             newData.UnitInStock = newData.UnitInStock - $scope.CountToBuy;
             $http.post("http://localhost:2342/api/Product/UpdateProduct/0", newData).success(function (result) {
                 $scope.CardData = result;
                 $scope.vsChangeInfo = true;
             });
         });
    };

    $scope.wks = { number: 0, validity: true }
    $http.get("http://localhost:2342/api/Product/AllProducts/2")
         .success(function (data) {
             $scope.AllProducts = data;
         });
    $scope.Price = 0;
    $scope.CountToBuy = 0;
    $scope.Inc = function (_price) {
        $scope.CountToBuy++;
        $scope.Price = $scope.CountToBuy * _price;
        $scope.vsBuyButton = false;
        $scope.vsChangeInfo = false;
    };
    $scope.Dec = function (_price) {
        if ($scope.CountToBuy > 0)
            $scope.CountToBuy--;
        $scope.Price = $scope.CountToBuy * _price;

        $scope.vsChangeInfo = false;
    };
    $scope.vsStokError = false;
    $scope.CheckCount = function (productId, count) {
        $http.get("http://localhost:2342/api/Product/ProductById/" + productId)
         .success(function (data) {

             $scope.vsChangeInfo = false;
             if (data.UnitInStock < count) {
                 $scope.vsStokError = true;
                 $scope.vsMoney = false;
             }
             else {
                 $scope.vsStokError = false;
                 $scope.vsMoney = true;
             }
         });
    };
    $scope.vsChangeInfo = false;
    $scope.vsBuyButton = false;
    $scope.vsMoneyError = false;
    $scope.CheckMoney = function (money) {

        $scope.vsChangeInfo = false;
        if ($scope.Price > money) {
            $scope.vsMoneyError = true;
            $scope.vsBuyButton = false;
        }
        else {
            $scope.vsMoneyError = false;
            $scope.vsBuyButton = true;
        }
    }


    $scope.vsCard = false;

    $scope.Show = function (productId) {
        $scope.vsCard = true;
        $scope.CountToBuy = 0;
        $scope.Price = 0;
        $scope.vsStokError = false;
        $http.get("http://localhost:2342/api/Product/ProductById/" + productId)
         .success(function (data) {
             $scope.CardData = data;

             $scope.vsChangeInfo = false;
         });
    }
    $scope.vsMoney = false;
    $scope.Hide = function (productId) {
        $scope.vsCard = false;
        $scope.CountToBuy = 0;
        $scope.Price = 0;
        $scope.vsStokError = false;
        $scope.vsMoney = false;
        $scope.vsChangeInfo = false;
    }

});