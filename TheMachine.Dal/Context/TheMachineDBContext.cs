namespace TheMachine.Dal.Context
{
    using Entities.EF;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class TheMachineDBContext : DbContext
    {

        public TheMachineDBContext() 
            : base("TheMachineDBContext")
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Product>()
            //      .HasOptional(p => p.Category)
            //      .WithMany(t => t.Products);

            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}