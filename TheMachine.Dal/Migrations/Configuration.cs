namespace TheMachine.Dal.Migrations
{
    using Entities.EF;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TheMachine.Dal.Context.TheMachineDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(TheMachine.Dal.Context.TheMachineDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.Categories.AddOrUpdate(
              p => p.ID,
              new Category { CategoryName = "Chocolate" },
              new Category { CategoryName = "Chips" },
              new Category { CategoryName = "Marshmallow" }
            );
            context.Products.AddOrUpdate(
              p => p.ID,
              new Product {
                  ProductName = "Doritos 300gr",
                  ProductImage = "http://www.clipartkid.com/images/303/doritos-bag-png-prices-want-to-buy-trad-sBscYl-clipart.png",
                  UnitInStock = 4,
                  UnitPrice = Convert.ToDecimal(1.80),
                  Category_ID = 2
              },
              new Product
              {
                  ProductName = "Lays 300gr",
                  ProductImage = "http://irecommend.ru/sites/default/files/product-images/10297/cc17159a8640262f8b2b36b68b8b447a.png",
                  UnitInStock = 0,
                  UnitPrice = Convert.ToDecimal(1.90),
                  Category_ID = 2
              },
               new Product
               {
                   ProductName = "Haribo 200gr",
                   ProductImage = "http://caravan.by/assets/upload/sweets/haribo/haribo-chamallows-speckies.png",
                   UnitInStock = 5,
                   UnitPrice = Convert.ToDecimal(1.60),
                   Category_ID = 3
               },
               new Product
               {
                   ProductName = "Snickers",
                   ProductImage = "https://upload.wikimedia.org/wikipedia/en/3/3c/Snickers_wrapped.png",
                   UnitInStock = 5,
                   UnitPrice = Convert.ToDecimal(0.50),
                   Category_ID = 1
               }
            );

        }
    }
}
