﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TheMachine.Dal.Repositories;

namespace TheMachine.Service.Repositories
{
    public interface IServiceRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        /// <summary>
        /// You can send linq request.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);
        T GetById(int id);
        T Get(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int ID);
    }
}
