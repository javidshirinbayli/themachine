﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TheMachine.Dal.Context;
using TheMachine.Dal.Repositories;
using TheMachine.Entities.EF;

namespace TheMachine.Service.Repositories
{
    public class ServiceRepository<T> : IServiceRepository<T> where T : class
    {
        TheMachineDBContext dbContext;
        IGenericRepository<T> _repository;
        public ServiceRepository()
        {
            dbContext = new TheMachineDBContext();
            _repository = new GenericRepository<T>(dbContext);
        }

        public void Add(T entity)
        {
            _repository.Add(entity);
        }

        public void Delete(int ID)
        {
            _repository.Delete(ID);
        }

        public void Delete(T entity)
        {
            _repository.Delete(entity);
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
           return _repository.Get(predicate);
        }

        public IQueryable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _repository.GetAll(predicate);
        }

        public T GetById(int id)
        {
            return _repository.GetById(id);
        }

        public void Update(T entity)
        {
            _repository.Update(entity);
        }
        
    }
}
