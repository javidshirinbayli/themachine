﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using TheMachine.Entities.EF;
using TheMachine.Service.Repositories;

namespace TheMachine.WebApi.api
{
    public class ProductController : ApiController
    {
        private IServiceRepository<Product> _productRepository;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ProductController()
        {
            _productRepository = new ServiceRepository<Product>();
        }

        [HttpGet]
        public IQueryable<Product> AllProducts()
        {
            Log.Debug("GET Request traced");
            return _productRepository.GetAll();
            
        }

        [HttpGet]
        public Product ProductById(int id)
        {
            Log.Debug("GET Request traced");
            return _productRepository.GetById(id);
        }

        [HttpPost]
        public Product UpdateProduct(Product product)
        {
            Log.Debug("Post Request traced");
            _productRepository.Update(product);
            return _productRepository.GetById(product.ID);            
        }


    }
}
